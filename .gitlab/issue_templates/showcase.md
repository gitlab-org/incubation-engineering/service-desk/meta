<!-- Title: Service Desk SEG 16.6 Showcase (November 2023) -->

Video-Link: [TBD]

**TL;DR**

Summary of the contents. Also suitable for LinkedIn etc.

**Table of contents**

1. One
1. Two
1. Goals

Our goal is to provide a complete customer support solution that integrates with the GitLab ecosystem and brings customers, support staff and developers closer together. My current focus is the custom email address feature.

## Merge requests for the milestone

This is the list of all merge requests for the milestone, you can find the link in the video description and the showcase issue:

https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=merged&assignee_username=msaleiko&milestone_title=16.6

## One

## Two

## Goals

## Goals from previous month

1. :heavy_check_mark:|:no_entry_sign:

## Goals for XY.Z

1. One
1. Two

/label ~"Incubation::Service Desk" 