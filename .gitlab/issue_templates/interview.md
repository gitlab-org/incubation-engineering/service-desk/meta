<!-- Use "Product interview - [Name]" as title -->

## What is your company and why did you choose Service Desk?

## How do you use Service Desk?

## What does your current workflow look like? How do you work with Service Desk?

## Would you test experimental or beta features in production?

## Did you know you can use a custom email address for Service Desk?

## Did you know you can add automation features to your Service Desk today with gitlab-triage?

## If you had a magic wand and could wish for a feature in Service Desk, what would it be?

/label ~"user interview"
/confidential