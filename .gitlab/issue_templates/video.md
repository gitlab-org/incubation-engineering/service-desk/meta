<!-- Title: Video - Video title -->

_Goal of the video._

Video-Link: \[TBD\]

## Short description

_Description used for YouTube, LinkedIn etc._

## Content

_Material collection for the video._

## Script

_Video script used for recording._

/label ~video ~"status::pending"
/assign me