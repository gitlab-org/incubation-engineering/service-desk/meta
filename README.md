# Service Desk Single-Engineer-Group Meta Project

This project is the single source of truth (SSOT) for all updates and showcases of the Service Desk SEG. [Browse the issues](https://gitlab.com/gitlab-org/incubation-engineering/service-desk/meta/-/issues) for written versions of all update 
 and showcase videos.

 - [Updates issue with a short description of every update and video link](https://gitlab.com/gitlab-org/incubation-engineering/service-desk/meta/-/issues/3)
 - [Exploration issue for collecting ideas](https://gitlab.com/gitlab-org/incubation-engineering/service-desk/meta/-/issues/2)

 See also the [Service Desk SEG handbook page](https://about.gitlab.com/handbook/engineering/development/incubation/service-desk/) as an entry point into the work of this SEG.

 The Service Desk SEG is part of the [Incubation Engineering Department](https://about.gitlab.com/handbook/engineering/development/incubation/).